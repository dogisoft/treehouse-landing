$(document).on("touchstart, click", ".hamburger", function(e){

  if($(".mobile-menu").is(':visible')){
    $(".mobile-menu").slideUp();
  }
  else{
    $(".mobile-menu").slideDown();
  }
})

$(document).on("touchstart, click", ".search-bar-mobile", function(e){

  if($(".search-on-mobile").is(':visible')){
    $(".search-on-mobile").slideUp();
  }
  else{
    $(".search-on-mobile").slideDown();
  }
})
